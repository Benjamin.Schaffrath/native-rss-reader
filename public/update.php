<?php
include_once __DIR__ . '/../config.php';

/*use Jtl\ElasticLogger\Config;
use Jtl\ElasticLogger\Logger;

Logger::init(
    new Config([
        'channel' => 'ben-rss',
        'host' => '78.47.29.64',
        'index' => 'logs'
    ])
);*/

$feeds = (new \App\RssFeed())->where([
    [
        'last_updated',
        '<=',
        sprintf("timestamp '%s' - interval '5' minute", date("Y-m-d H:i:s"))
    ]
]);

/** @var \App\RssFeed $feed */
foreach ($feeds as $feed) {
    /*Logger::debug('Starting to parse: ' . $feed->getData('title'), [
        'title' => $feed->getData('title'),
        'rss-url' => $feed->getData('rss_url'),
        'rss-id' => $feed->getID()
    ]);*/
    
    $feed->parse();
}

/*Logger::debug('Finished parsing');*/
