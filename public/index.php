<?php
session_start();
include_once '../config.php';
include_once '../posthandler.php';

$userFeeds = App\Services\RssFeedService::getFeedsForUser();
?>
<html>
<head>
<?php include_once '../resources/head.html' ?>
</head>
<body class="bg-dark">
<?php include_once '../resources/navbar.php' ?>

<div class="container-fluid">
    <div class="row" id="feedContainer">
        <?php for ($i = 0; $i < count($userFeeds); $i++) {
            $entries = $userFeeds[$i]->getEntries(27);
            include '../resources/rss-feed.php';
        } ?>
    </div>
</div>
<?php include_once '../resources/login-modal.php'; ?>
<?php include_once '../resources/register-modal.php'; ?>
<?php include_once '../resources/settings-modal.php'; ?>
<?php include_once '../resources/new-feed-modal.php'; ?>
<?php include_once '../resources/remove-feed-modal.php'; ?>
</body>
</html>
