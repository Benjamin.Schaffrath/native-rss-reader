$(document).ready(function () {
    var lastPressedRemoveButton = null;
    var hasTypedInSearchBox = false;

    initialize();
    setTimeout(updatePage, 20000);

    function updatePage() {
        if (hasTypedInSearchBox || $('.modal.show').length > 0 || !document.hidden) {
            hasTypedInSearchBox = false;
            setTimeout(updatePage, 60000);
        } else {
            location.reload();
        }
    }

    function initialize() {
        let feedsPerRow = localStorage.getItem('feedsPerRow');
        let feedHeight = localStorage.getItem('feedHeight');
        let colSize = null;

        $('[data-toggle="tooltip"]').tooltip();

        if (feedsPerRow == null) {
            feedsPerRow = 3;
        }

        switch (feedsPerRow) {
            case "2":
                colSize = 6;
                break;
            case "4":
                colSize = 3;
                break;
            case "6":
                colSize = 2;
                break;
            case "3":
            default:
                colSize = 4;
        }

        $('.card-body').css('height', feedHeight + 'vh');

        $('.rss-feed').removeClass(function (index, className) {
            return (className.match(/\bcol-sm-\S+/g) || []).join(' ');
        });

        $('.rss-feed').addClass('col-sm-' + colSize);

        $('input[name=feedsPerRow][value=' + feedsPerRow + ']').prop('checked', true);
        $('input[name=feedHeight][value=' + feedHeight + ']').prop('checked', true);

    }

    $('.google-input').on("change", function (event) {
        hasTypedInSearchBox = true;
    });

    $('#loginForm').on('submit', function(event) {
        event.preventDefault();

        let email = $('#emailLogin').val();
        let password = $('#passwordLogin').val();

        if (email === '' || password === '') {
            return;
        }

        let parameters = {
            'email': email,
            'password': password,
            'action': 'login'
        };
        $.post(
            "index.php", parameters,
            function (data) {
                location.reload();
            }
        ).fail(function () {
            alert('Invalid credentials!');
        });
    });

    $('#registerForm').on("submit", function (event) {
        event.preventDefault();

        let email = $('#emailRegister').val();
        let password = $('#passwordRegister').val();

        if (email === '' || password === '') {
            return;
        }

        let parameters = {
            'email': email,
            'password': password,
            'action': 'register'
        };
        $.post(
            "index.php", parameters,
            function (data) {
                location.reload();
            }
        ).fail(function () {
            alert('Error on registering!');
        });
    });

    $('#saveSettingsButton').on("click", function (event) {
        event.preventDefault();

        let feedsPerRow = $('input[name=feedsPerRow]:checked').val();
        let feedHeight = $('input[name=feedHeight]:checked').val();

        if (feedsPerRow) {
            localStorage.setItem('feedsPerRow', feedsPerRow);
        }

        if (feedHeight) {
            localStorage.setItem('feedHeight', feedHeight);
        }

        $('#settingsModal').modal('toggle');

        initialize();
    });

    $(".google-input").on('keyup', function (e) {
        if (e.keyCode == 13) {
            location.href = 'https://www.google.de/search?q=' + $(this).val();
        }
    });

    $('#feedForm').on("submit", function (event) {
        event.preventDefault();

        let rssUrl = $('#rss-url').val();

        if (rssUrl === '') {
            return;
        }

        let parameters = {
            'rssUrl': rssUrl,
            'action': 'addRss'
        };
        $.post(
            "index.php", parameters,
            function (data) {
                location.reload();
            }
        ).fail(function () {
            alert('Error on subscribing to the feed!');
        });
    });

    $('#confirmRemoveRssButton').on("click", function (event) {
        event.preventDefault();

        let parameters = {
            'rssToRemove': lastPressedRemoveButton,
            'action': 'removeRss'
        };
        $.post(
            "index.php", parameters,
            function (data) {
                location.reload();
            }
        ).fail(function () {
            alert('Error while removing the feed!');
        });
    });

    $('.remove-button').on("click", function (event) {
        lastPressedRemoveButton = $(this).attr('data-feed-id');
    });

    $('#logoutButton').on("click", function (event) {
        let parameters = {
            'action': 'logout'
        };

        $.post(
            "index.php", parameters,
            function (data) {
                location.reload();
            }
        ).fail(function () {
            alert('Error while logging out!');
        });
    });

    let items = Sortable.create(feedContainer, {
        group: "feed_sort",
        store: {
            get: function (sortable) {
                var order = localStorage.getItem(sortable.options.group.name);
                return order ? order.split('|') : [];
            },

            set: function (sortable) {
                var order = sortable.toArray();
                localStorage.setItem(sortable.options.group.name, order.join('|'));
            }
        },
        handle: ".card-header",
        animation: 150
    });
});
