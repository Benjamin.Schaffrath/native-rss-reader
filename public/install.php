<?php
include_once '../config.php';

$oDBH = \App\Services\DatabaseService::getInstance();

$success = $oDBH->exec(file_get_contents(sprintf('%s%s..%sinitial_migration.sql', __DIR__, DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR)));

if ($success === false) {
    header("HTTP/1.0 500 Internal Server Error");
    exit('<span color="red">Error while executing the .SQL file!</span>');
}

exit('<span color="green">Success!</span>');
