<?php
include_once 'config.php';

use App\Services\AuthService;
use App\User;

$auth = AuthService::getInstance();

if (!empty($_POST)) {
    if (!empty($_POST['email']) && !empty($_POST['password'])) {
        if ($_POST['action'] == 'login') {
            try {
                $auth->login($_POST['email'], $_POST['password'], 31557600);
            } catch (\Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
            }
            exit();
        } elseif ($_POST['action'] == 'register') {
            try {
                $auth->register($_POST['email'], $_POST['password']);
                $auth->login($_POST['email'], $_POST['password'], 31557600);
            } catch (\Exception $e) {
                header("HTTP/1.0 500 Internal Server Error");
            }
            exit();
        }
    } elseif ($auth->isLoggedIn()) {
        $user = new User();
        $user->setID($auth->id());
        $user->load();
        
        if (!empty($_POST['action']) && $_POST['action'] == 'settings') {
            if (!empty($_POST['feedsPerRow']) && ($_POST['feedsPerRow'] >= 2 && $_POST['feedsPerRow'] <= 6 && $_POST['feedsPerRow'] != 5 )) {
                $_SESSION['feedsPerRow'] = $_POST['feedsPerRow'];
            }
            if (!empty($_POST['feedHeight']) && ($_POST['feedHeight'] == 30 || $_POST['feedHeight'] == 60 || $_POST['feedHeight'] == 90)) {
                $_SESSION['feedHeight'] = $_POST['feedHeight'];
            }
            
            
            exit();
        }
        
        if (!empty($_POST['action']) && $_POST['action'] == 'logout') {
            try {
                AuthService::getInstance()->logOut();
            } catch (\Delight\Auth\AuthError $e) {
                header("HTTP/1.0 500 Internal Server Error");
            }
            
            exit();
        }
        
        if (!empty($_POST['action']) && $_POST['action'] == 'addRss' && !empty($_POST['rssUrl'])) {
            if (!$user->subscribeToFeed($_POST['rssUrl'])) {
                header("HTTP/1.0 500 Internal Server Error");
            }
            
            exit();
        }
        
        if (!empty($_POST['action']) && $_POST['action'] == 'removeRss' && !empty($_POST['rssToRemove'])) {
            if (!$user->unsubscribeFromFeed($_POST['rssToRemove'])) {
                header("HTTP/1.0 500 Internal Server Error");
            }
            
            exit();
        }
    }
    header("HTTP/1.0 422 Unprocessable Entity");
    exit();
}
