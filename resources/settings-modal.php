<div class="modal fade" id="settingsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Settings</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="feedPerRow">Feeds per Row</label>
                    <div id="feedPerRow">
                        <label class="radio-inline"><input type="radio" value="2" name="feedsPerRow">2</label>
                        <label class="radio-inline"><input type="radio" value="3" name="feedsPerRow">3</label>
                        <label class="radio-inline"><input type="radio" value="4" name="feedsPerRow">4</label>
                        <label class="radio-inline"><input type="radio" value="6" name="feedsPerRow">6</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="feedHeight">Feed height</label>
                    <div id="feedHeight">
                        <label class="radio-inline"><input type="radio" value="30" name="feedHeight">30%</label>
                        <label class="radio-inline"><input type="radio" value="60" name="feedHeight">60%</label>
                        <label class="radio-inline"><input type="radio" value="87" name="feedHeight">87%</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="saveSettingsButton" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
