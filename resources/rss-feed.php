<div class="col-sm-4 pb-4 rss-feed" data-id="<?php echo $userFeeds[$i]->getId() ?>">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <a href="<?php echo $userFeeds[$i]->getData('href') ?>">
                <img class="feed-img" src="<?php echo $userFeeds[$i]->getData('img_url') ?>">
            </a>
            <button type="button" class="btn btn-outline-danger btn-sm remove-button" data-toggle="modal"
                    data-target="#removeFeedModal" data-feed-id="<?php echo $userFeeds[$i]->getId() ?>">Remove
            </button>
        </div>
        <div class="card-body" style="height: 60vh">
            <h5 class="card-title"><?php echo $userFeeds[$i]->getData('description') ?></h5>
            <?php foreach ($entries as $entry): ?>
                <?php if ($entry->getData('img_url') != null) { $useImage = true; } else { $useImage = false; } ?>
                <hr>
                
                <a href="<?php echo $entry->getData('href') ?>" data-toggle="tooltip" title="<?php echo htmlspecialchars($entry->getData('title')) ?>">
                    <div class="row">
                    <?php if ($useImage == true) {
                        echo '<div class="col-sm-2">';
                        echo '<img class="feed-entry-image img-responsive" src="' . $entry->getData("img_url") . '">';
                        echo '</div>';
                        echo '<div class="col-sm-9 feed-text-col">';
                    } else {
                        echo '<div class="col-sm-12 feed-text-col">';
                    }?>
                    
                    <span class="card-text"><?php echo $entry->getData('title') ?></span>
                    </div>
        </div>
                </a>
        
            <?php endforeach; ?>
        </div>
    </div>
</div>
