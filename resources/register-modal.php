<div class="modal fade" id="registerModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Register</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="registerForm">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="emailRegister">E-mail</label>
                        <input name="login" class="form-control" id="emailRegister" placeholder="E-Mail">
                    </div>
                    <div class="form-group">
                        <label for="passwordRegister">Password</label>
                        <input name="password" class="form-control" type="password" id="passwordRegister" placeholder="Password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Register</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
