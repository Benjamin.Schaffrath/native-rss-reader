<nav class="navbar navbar-expand navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <?php
                if (App\Services\AuthService::getInstance()->isLoggedIn()) {
                    echo '<button type="button" class="btn mini-button" data-toggle="modal" data-target="#newFeedModal"><i class="fas fa-plus icon"></i></button>';
                }
                ?>
            <input class="google-input" placeholder="Google Search!" autofocus>
        <?php
        if (App\Services\AuthService::getInstance()->isLoggedIn()) {
            echo '<button type="button" class="btn mini-button mr-1" data-toggle="modal" data-target="#settingsModal"><i class="fas fa-user-cog icon"></i></button>';
            echo '<button type="button" class="btn mini-button" id="logoutButton"><i class="fas fa-sign-out-alt icon"></i></button>';
        } else {
            echo '<button type="button" class="btn mini-button mr-1" data-toggle="modal" data-target="#loginModal"><i class="fas fa-sign-in-alt icon"></i></button>';
            echo '<button type="button" class="btn mini-button" data-toggle="modal" data-target="#registerModal"><i class="fas fa-user-plus icon"></i></button>';
        }
        ?>

    </div>
</nav>
