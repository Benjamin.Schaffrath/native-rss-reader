<div class="modal fade" id="removeFeedModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Remove the RSS Feed?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to remove the subscription to the RSS Feed? <br>
                (You can subscribe back to it later)
            </div>
            <div class="modal-footer">
                <button type="button" id="confirmRemoveRssButton" class="btn btn-primary">Remove</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
