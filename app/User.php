<?php


namespace App;

use App\Services\AuthService;
use App\Database\DataObject;

class User extends DataObject
{
    protected $tableName = 'users';
    
    /**
     * @param string $rssUrl
     * @return bool
     */
    public function subscribeToFeed(string $rssUrl): bool
    {
        $feed = (new RssFeed())->where([['rss_url', '=', '"' . $rssUrl . '"']]);
        $feed = reset($feed);
    
        if (empty($feed)) {
            $feed = new RssFeed();
            $feed->setData('rss_url', $_POST['rssUrl']);
            $feed->save();
            $feed->parse();
        }
        
        $subscriptionEntry = new RssFeedsUsers();
        $subscriptionEntry->setData('rss_feed_id', $feed->getID());
        $subscriptionEntry->setData('user_id', AuthService::getInstance()->id());

        return $subscriptionEntry->save();
    }
    
    public function unsubscribeFromFeed($rssToRemove)
    {
        $feed = (new RssFeedsUsers())->where([
            [
                'rss_feed_id',
                '=',
                '"' . $rssToRemove . '"'
            ],
            [
                'user_id',
                '=',
                AuthService::getInstance()->id()
            ]
            ]);

        /** @var RssFeedsUsers $feed */
        $feed = reset($feed);
        
        if (!$feed) {
            return false;
        }
        
        if ($feed->delete()) {
            return true;
        }
        
        return false;
    }
}
