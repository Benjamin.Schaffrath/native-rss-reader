<?php

namespace App\Services;

use App\RssFeed;

class RssFeedService
{
    public static function getFeedsForUser()
    {
        if (!AuthService::getInstance()->isLoggedIn()) {
            return [];
        }
        
        $userID = AuthService::getInstance()->id();
        
        $oDBH = DatabaseService::getInstance();
        $query = '
            SELECT
                *
            FROM
                `rss_feeds_users`
            WHERE
                user_id = ?
            ';
        $cmd = $oDBH->prepare($query);
        $cmd->execute([$userID]);
        $entries = array();
        while ($row = $cmd->fetch()) {
            $entry = new RssFeed();
            $entry->setID($row['rss_feed_id']);
            if (!$entry->load()) {
                continue;
            }
            $entries[] = $entry;
        }
        return $entries;
    }
}
