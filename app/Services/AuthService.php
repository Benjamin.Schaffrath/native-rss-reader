<?php

namespace App\Services;

class AuthService
{
    /**
     * @var \Delight\Auth\Auth
     */
    private static $auth;
    
    /**
     * @return \Delight\Auth\Auth
     */
    public static function getInstance()
    {
        if (!self::$auth) {
            self::_createInstance();
        }
        
        return self::$auth;
    }
    
    private static function _createInstance()
    {
        self::$auth = new \Delight\Auth\Auth(DatabaseService::getInstance());
    }
}
