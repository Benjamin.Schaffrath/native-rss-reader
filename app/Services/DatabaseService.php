<?php

namespace App\Services;

use PDO;

class DatabaseService
{
    /**
     * @var PDO
     */
    private static $oDBH;
    
    /**
     * @return PDO
     */
    public static function getInstance()
    {
        if (!self::$oDBH) {
            self::_createInstance();
        }
        
        return self::$oDBH;
    }
    
    private static function _createInstance()
    {
        self::$oDBH = new PDO(DBSTRING, DBUSER, DBPASSWORD);
    }
}
