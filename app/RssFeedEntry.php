<?php


namespace App;

use App\Database\DataObject;

class RssFeedEntry extends DataObject
{
    protected $tableName = 'rss_feed_entries';
}
