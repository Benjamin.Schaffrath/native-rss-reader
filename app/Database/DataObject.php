<?php

namespace App\Database;

use App\Services\DatabaseService;
use PDO;

abstract class DataObject
{
    protected $id;
    protected $tableName;
    protected $data = array();

    public function all(): array
    {
        $class = get_called_class();
        $oDBH = DatabaseService::getInstance();
        $query = '
            SELECT
                *
            FROM
                `' . $this->tableName . '`
            ';
        $cmd = $oDBH->prepare($query);
        $cmd->execute();
        $entries = array();
        while ($row = $cmd->fetch()) {
            $entry = new $class();
            $entry->setID($row['id']);
            if (!$entry->load()) {
                continue;
            }
            $entries[] = $entry;
        }
        return $entries;
    }
    
    /**
     * @param int $setID
     */
    public function setID($setID): self
    {
        $this->id = $setID;
        
        return $this;
    }

    /**
     * @return int
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getData($key)
    {
        if (!array_key_exists($key, $this->data)) {
            throw new \InvalidArgumentException();
        }
        return $this->data[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setData($key, $value)
    {
        if ($value == []) {
            $this->data[$key] = '';
            
            return;
        }
        
        $this->data[$key] = $value;
    }

    /**
     * @return boolean
     */
    public function load()
    {
        $oDBH = DatabaseService::getInstance();
        $query = '
            SELECT
                *
            FROM
                `' . $this->tableName . '`
            WHERE
                `id` = ?
            ';
        $cmd = $oDBH->prepare($query);
        $cmd->execute(array($this->id));
        $row = $cmd->fetch(PDO::FETCH_ASSOC);
        if ($row == false) {
            return false;
        }

        $this->data = $row;
        return true;
    }
    
    public function where(array $arguments): array
    {
        $oDBH = DatabaseService::getInstance();
        $class = get_called_class();
        
        $where = '';
        foreach ($arguments as $argument) {
            if (!is_array($argument) || count($argument) != 3) {
                throw new \InvalidArgumentException();
            }
            $where .= 'AND ' . implode(' ', $argument);
        }
        
        $query = '
            SELECT
                *
            FROM
                ' . $this->tableName . '
            WHERE
                1=1 ' . $where;

        $cmd = $oDBH->prepare($query);
        $cmd->execute([$this->id]);
        $entries = [];
        while ($row = $cmd->fetch()) {
            $entry = new $class();
            $entry->setID($row['id']);
            if (!$entry->load()) {
                continue;
            }
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * @return boolean
     */
    public function save()
    {
        if ($this->getID()) {
            return $this->update();
        } else {
            return $this->insert();
        }
    }

    /**
     * @return bool
     */
    private function insert()
    {
        $oDBH = DatabaseService::getInstance();
        $question = array_map(
            function () {
                return '?';
            },
            $this->data
        );
        $implode = implode(',', $question);
        $query = '
            INSERT INTO
                `' . $this->tableName . '` (' . implode(',', array_keys($this->data)) . ')
            VALUES
                (' . $implode . ')
        ';

        $cmd = $oDBH->prepare($query);
        if ($cmd->execute(array_values($this->data))) {
            $this->id = $oDBH->lastInsertId();
            
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function update()
    {
        $oDBH = DatabaseService::getInstance();
        foreach ($this->data as $key => $value) {
            $set[] = $key . '=?';
        }

        $query = '
            UPDATE
                `' . $this->tableName . '`
            SET
                ' . implode(',', $set) . '
            WHERE
                id=' . $this->getID() . '
        ';

        $cmd = $oDBH->prepare($query);
        if ($cmd->execute(array_values($this->data))) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function delete()
    {
        $oDBH = DatabaseService::getInstance();

        $query = '
            DELETE FROM ' . $this->tableName . '
            WHERE id = ?';

        $cmd = $oDBH->prepare($query);
        if ($cmd->execute(array($this->getID()))) {
            return true;
        }
        return false;
    }
}
