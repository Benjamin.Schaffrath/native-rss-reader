<?php


namespace App;

use App\Services\DatabaseService;
use App\Database\DataObject;
/*use Jtl\ElasticLogger\Config;
use Jtl\ElasticLogger\Logger;*/
use PDO;

class RssFeed extends DataObject
{
    protected $tableName = 'rss_feeds';
    protected $nsprefix = null;
    protected $xml = null;
    
    public function getEntries($entries = 5)
    {
        if (!$this->id) {
            return [];
        }
        
        $oDBH = DatabaseService::getInstance();
        $oDBH->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        
        $query = sprintf('
            SELECT
                *
            FROM
                rss_feed_entries
            WHERE
                rss_feed_id = ?
            ORDER BY id DESC
            LIMIT ?
            '
        );
        $cmd = $oDBH->prepare($query);
        $cmd->execute([$this->id, $entries]);
        $entries = [];
        while ($row = $cmd->fetch()) {
            $entry = new RssFeedEntry();
            $entry->setID($row['id']);
            if (!$entry->load()) {
                continue;
            }
            $entries[] = $entry;
        }
        
        return $entries;
    }
    
    public function parse()
    {
        /*Logger::init(
            new Config([
                'channel' => 'ben-rss',
                'host' => '78.47.29.64',
                'index' => 'logs'
            ])
        );*/
        
        if (!$this->getID()) {
            return;
        }
        
        @$this->xml = simplexml_load_file($this->getData('rss_url'), 'SimpleXMLElement', LIBXML_NOCDATA);
        
        if ($this->xml == false) {
            $this->setData('title', "Couldn't get rss_feed");
            $this->setData('description', "Couldn't get rss_feed");
            $this->setData('href', "Couldn't get rss_feed");
            $this->setData('img_url',
                'http://via.placeholder.com/160x40/ffffff/0000000000?text=' . "Couldn't get rss_feed");
            $this->save();
    
            try {
                /*Logger::error("Couldn't parse the rss feed!", [
                    'rss-url' => $this->getData('rss_url'),
                    'rss-id' => $this->getID()
                ]);*/
            } catch (\Exception $e) {
            }
    
            return;
        }
        
        $this->nsprefix = $this->getPrefix();
        
        $this->parseTitle();
        
        $this->parseDescription();
        
        $this->parseHref();
    
        $this->parseImgUrl();
        
        $this->setData('last_updated', date("Y-m-d H:i:s"));
        
        if ($this->save()) {
            /*Logger::debug('Successfully parsed: ' . $this->getData('title') . ' Now parsing children!', [
                'title' => $this->getData('title'),
                'rss-url' => $this->getData('rss_url'),
                'rss-id' => $this->getID()
            ]);*/
            
            return $this->parseChilds();
        }
    }
    
    private function parseTitle()
    {
        $title = $this->xml->title;
        if (empty($title)) {
            $title = $this->xml->channel->title;
        }
        
        $this->setData('title', (string)$title);
    }
    
    private function parseDescription()
    {
        $description = $this->xml->description;
        if (empty($description)) {
            $description = $this->xml->channel->description;
        }
        if (empty($description)) {
            $description = $this->xml->subtitle;
        }
        if (empty($description)) {
            $description = $this->xml->channel->subtitle;
        }
        if (empty($description)) {
            $description = $this->getData('title');
        }
        $this->setData('description', (string)$description);
    }
    
    private function parseHref()
    {
        $href = $this->xml->link;
        if (!filter_var($href, FILTER_VALIDATE_URL)) {
            $href = $this->xml->id;
        }
        if (!filter_var($href, FILTER_VALIDATE_URL)) {
            $href = $this->xml->channel->id;
        }
        if (!filter_var($href, FILTER_VALIDATE_URL)) {
            $href = $this->xml->channel->link;
        }
        
        $this->setData('href', (string)$href);
    }
    
    private function parseImgUrl()
    {
        $img = $this->xml->image->url;
        if (!filter_var($img, FILTER_VALIDATE_URL)) {
            $img = $this->xml->channel->image->url;
        }
        if (!filter_var($img, FILTER_VALIDATE_URL)) {
            $img = $this->xml->channel->logo;
        }
        if (!filter_var($img, FILTER_VALIDATE_URL)) {
            $img = $this->xml->channel->icon;
        }
        if (!filter_var($img, FILTER_VALIDATE_URL)) {
            $img = $this->xml->logo;
        }
        if (!filter_var($img, FILTER_VALIDATE_URL)) {
            $img = $this->xml->icon;
        }
        $this->setData('img_url', (string)$img);
    
        if ($this->getData('img_url') == '') {
            $this->setData('img_url',
                'http://via.placeholder.com/160x40/ffffff/0000000000?text=' . $this->getData('title'));
        }
    }
    
    private function parseChilds()
    {
        $xpathItemQueries = [
            '//' . $this->nsprefix . 'item',
            '//' . $this->nsprefix . 'entry',
        ];
        
        $items = [];
        foreach ($xpathItemQueries as $query) {
            $items = $this->xml->xpath($query);
            
            if (!empty($items)) {
                break;
            }
        }
        
        /** @var \SimpleXMLElement $item */
        foreach ($items as $item) {
            $title = (string)$item->title;
            
            $href = (string)$this->getNestedData($item->link);
            if (!filter_var($href, FILTER_VALIDATE_URL)) {
                $href = (string)$this->getNestedData($item->id);
            }
            
            if (empty($title) || empty($href)) {
                continue;
            }
            
            $found = $this->parseChildThumbnail($item);
            
            $rssFeedEntry = new RssFeedEntry();
            $rssFeedEntry->setData('title', (string)$title);
            $rssFeedEntry->setData('href', (string)$href);
            if ($found) {
                $rssFeedEntry->setData('img_url', (string)$found);
            }
            $rssFeedEntry->setData('rss_feed_id', $this->getID());
            
            $update = (new RssFeedEntry())->where([
                [
                    'href',
                    '=',
                    '"' . $href . '"',
                ],
            ]);
            
            if (!empty($update)) {
                $rssFeedEntry->setID($update[0]->getId());
            }
            
            if ($rssFeedEntry->save()) {
                /*Logger::debug('Found new news: ' . $rssFeedEntry->getData('title'), [
                    'title' => $rssFeedEntry->getData('title'),
                    'rss-feed-url' => $rssFeedEntry->getData('href'),
                    'rss-feed-id' => $rssFeedEntry->getID()
                ]);*/
            } else {
                /*Logger::error("Couldn't save an rss feed entry for feed: " . $this->getData('title'), [
                    'title' => $this->getData('title'),
                    'rss-url' => $this->getData('rss_url'),
                    'rss-id' => $this->getID()
                ]);*/
            }
        }
    }
    
    private function parseChildThumbnail($item)
    {
        $imgRegex = '/<img.*?src="([^"]*)"/';
        $foundImg = null;
        
        $content = $item->children("content", true);
        $found = preg_match($imgRegex, $content, $foundImg);
        if ($found) {
            return $foundImg[1];
        }
    
        $content = $item->thumbnail;
        $found = preg_match($imgRegex, $content, $foundImg);
        if ($found) {
            return $foundImg[1];
        }
    
        $content = $item->description;
        $found = preg_match($imgRegex, $content, $foundImg);
        if ($found) {
            return $foundImg[1];
        }
    
        $foundImg = $item->content->div->a->img->attributes();
        if (isset($foundImg[0])) {
            return (string)$foundImg[0];
        }

    
        return false;
    }
    
    private function getNestedData($element)
    {
        if (is_string($element)) {
            return $element;
        } elseif (is_array($element)) {
            if (isset($element[0])) {
                return $this->getNestedData($element[0]);
            }
        }
        
        if (reset($element) == false) {
            return $element;
        }
        
        return $this->getNestedData(reset($element));
    }
    
    private function getPrefix(): string
    {
        $namespaces = $this->xml->getDocNamespaces();
        $nsprefix = '';
        
        if (isset($namespaces[''])) {
            $defaultNamespaceUrl = $namespaces[''];
            $this->xml->registerXPathNamespace('default', $defaultNamespaceUrl);
            $nsprefix = 'default:';
        }
        
        return $nsprefix;
    }
}
